<?php

namespace WarehouseX\ClWarehouse;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getCartonCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\Carton\\CartonOutput[]',
        ],
        'postCartonCollection' => [
            '201.' => 'WarehouseX\\ClWarehouse\\Model\\Carton\\CartonOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'generate_referenceCartonCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\Carton\\CartonOutput[]',
        ],
        'import_by_fileCartonCollection' => [
            '201.' => 'WarehouseX\\ClWarehouse\\Model\\Carton\\CartonOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getCartonItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\Carton\\CartonOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putCartonItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\Carton\\CartonOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteCartonItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getInboundOrderDetailCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderDetail\\InboundOrderDetailOutput[]',
        ],
        'postInboundOrderDetailCollection' => [
            '201.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderDetail\\InboundOrderDetailOutput\\InboundOrderDetailGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getInboundOrderDetailItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderDetail\\InboundOrderDetailOutput\\InboundOrderDetailGetDetail',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putInboundOrderDetailItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderDetail\\InboundOrderDetailOutput\\InboundOrderDetailGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteInboundOrderDetailItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchInboundOrderDetailItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderDetail\\InboundOrderDetailOutput\\InboundOrderDetailGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getInboundOrderInstockRecordCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderInstockRecord[]',
        ],
        'postInboundOrderInstockRecordCollection' => [
            '201.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderInstockRecord',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getInboundOrderInstockRecordItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderInstockRecord',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putInboundOrderInstockRecordItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderInstockRecord',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteInboundOrderInstockRecordItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchInboundOrderInstockRecordItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderInstockRecord',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getInboundOrderReceiveRecordCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderReceiveRecord\\InboundOrderReceiveRecordOutput[]',
        ],
        'postInboundOrderReceiveRecordCollection' => [
            '201.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderReceiveRecord\\InboundOrderReceiveRecordOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getInboundOrderReceiveRecordItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderReceiveRecord\\InboundOrderReceiveRecordOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putInboundOrderReceiveRecordItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderReceiveRecord\\InboundOrderReceiveRecordOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteInboundOrderReceiveRecordItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchInboundOrderReceiveRecordItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderReceiveRecord\\InboundOrderReceiveRecordOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getInboundOrderCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput[]',
        ],
        'postInboundOrderCollection' => [
            '201.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'finish_instockInboundOrderCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput[]',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'finish_receiveInboundOrderCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput[]',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getInboundOrderItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putInboundOrderItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteInboundOrderItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchInboundOrderItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'api_inbound_orders_inbound_order_details_get_subresourceInboundOrderSubresource' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrderDetail\\InboundOrderDetailOutput[]',
        ],
        'instockInboundOrderItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'instock_by_cartonInboundOrderItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'receiveInboundOrderItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'receive_by_cartonInboundOrderItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\InboundOrder\\InboundOrderOutput\\InboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getOutboundOrderDetailCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundOrderDetail\\OutboundOrderDetailOutput[]',
        ],
        'getOutboundOrderDetailItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundOrderDetail\\OutboundOrderDetailOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getOutboundOrderCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundOrder\\OutboundOrderOutput[]',
        ],
        'postOutboundOrderCollection' => [
            '201.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundOrder\\OutboundOrderOutput\\OutboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getOutboundOrderItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundOrder\\OutboundOrderOutput\\OutboundOrderGetDetail',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putOutboundOrderItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundOrder\\OutboundOrderOutput\\OutboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteOutboundOrderItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchOutboundOrderItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundOrder\\OutboundOrderOutput\\OutboundOrderGetDetail',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'api_outbound_orders_outbound_order_details_get_subresourceOutboundOrderSubresource' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundOrderDetail\\OutboundOrderDetailOutput[]',
        ],
        'getOutboundPickingListItemCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundPickingListItem\\OutboundPickingListItemOutput[]',
        ],
        'getOutboundPickingListItemItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundPickingListItem\\OutboundPickingListItemOutput',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getOutboundPickingListCollection' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundPickingList\\OutboundPickingListOutput[]',
        ],
        'generateOutboundPickingListCollection' => [
            '201.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundPickingList\\OutboundPickingListOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getOutboundPickingListItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundPickingList\\OutboundPickingListOutput\\OutboundPickingListGetDetail',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putOutboundPickingListItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundPickingList\\OutboundPickingListOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteOutboundPickingListItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'pick_itemOutboundPickingListItem' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundPickingList\\OutboundPickingListOutput',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'api_outbound_picking_lists_picking_list_items_get_subresourceOutboundPickingListSubresource' => [
            '200.' => 'WarehouseX\\ClWarehouse\\Model\\OutboundPickingListItem\\OutboundPickingListItemOutput[]',
        ],
    ];
}
