<?php

namespace WarehouseX\ClWarehouse\Model\OutboundOrderDetail\OutboundOrderInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrderDetail.
 */
class OutboundOrderPut extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $quantity = null;

    /**
     * @var string
     */
    public $status = 'WAIT_PICKING';

    /**
     * @var \WarehouseX\ClWarehouse\Model\Carton\OutboundOrderInput\OutboundOrderPut
     */
    public $carton = null;
}
