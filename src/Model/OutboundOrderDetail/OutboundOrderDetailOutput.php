<?php

namespace WarehouseX\ClWarehouse\Model\OutboundOrderDetail;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrderDetail.
 */
class OutboundOrderDetailOutput extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $cartonId = null;

    /**
     * @var int
     */
    public $quantity = null;

    /**
     * @var string
     */
    public $status = 'WAIT_PICKING';

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var \WarehouseX\ClWarehouse\Model\Carton\OutboundOrderDetailOutput
     */
    public $carton = null;
}
