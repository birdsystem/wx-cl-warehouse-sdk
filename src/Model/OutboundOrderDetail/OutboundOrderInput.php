<?php

namespace WarehouseX\ClWarehouse\Model\OutboundOrderDetail;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrderDetail.
 */
class OutboundOrderInput extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $quantity = null;

    /**
     * @var \WarehouseX\ClWarehouse\Model\Carton\OutboundOrderInput
     */
    public $carton = null;
}
