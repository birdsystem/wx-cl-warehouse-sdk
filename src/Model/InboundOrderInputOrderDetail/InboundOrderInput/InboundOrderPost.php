<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrderInputOrderDetail\InboundOrderInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

class InboundOrderPost extends AbstractModel
{
    /**
     * @var string
     */
    public $cartonReference = null;

    /**
     * @var int
     */
    public $estimatedCartonQuantity = null;

    /**
     * @var string
     */
    public $cartonNote = null;
}
