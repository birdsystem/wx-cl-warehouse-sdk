<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrderInputOrderDetail\InboundOrderInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

class InboundOrderPut extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $cartonReference = null;

    /**
     * @var int
     */
    public $estimatedCartonQuantity = null;

    /**
     * @var string
     */
    public $cartonNote = null;
}
