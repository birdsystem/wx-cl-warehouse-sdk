<?php

namespace WarehouseX\ClWarehouse\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrderInstockRecord.
 */
class InboundOrderInstockRecord extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $containerReference = null;

    /**
     * @var int
     */
    public $cartonId = null;

    /**
     * @var int
     */
    public $userId = null;

    /**
     * @var int
     */
    public $stockedCartonQuantity = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $inboundOrder = null;
}
