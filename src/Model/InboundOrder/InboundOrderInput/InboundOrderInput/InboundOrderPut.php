<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderInput\InboundOrderInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrderPut extends AbstractModel
{
    /**
     * @var int
     */
    public $recordId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $warehouseId = null;

    /**
     * @var string|null
     */
    public $estimatedArrivalTime = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string|null
     */
    public $status = null;

    /**
     * @var
     * \WarehouseX\ClWarehouse\Model\InboundOrderInputOrderDetail\InboundOrderInput\InboundOrderPut[]
     */
    public $inboundOrderDetails = null;
}
