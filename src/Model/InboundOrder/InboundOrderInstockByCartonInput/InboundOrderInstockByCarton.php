<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderInstockByCartonInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrderInstockByCarton extends AbstractModel
{
    /**
     * @var string
     */
    public $cartonReference = null;

    /**
     * @var string
     */
    public $containerReference = null;

    /**
     * @var int
     */
    public $cartonQuantity = null;
}
