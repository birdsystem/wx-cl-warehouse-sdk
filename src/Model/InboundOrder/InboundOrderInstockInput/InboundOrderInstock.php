<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderInstockInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrderInstock extends AbstractModel
{
    /**
     * @var
     * \WarehouseX\ClWarehouse\Model\InboundOrderInstockInputOrderDetail\InboundOrderInstock[]
     */
    public $cartons = null;
}
