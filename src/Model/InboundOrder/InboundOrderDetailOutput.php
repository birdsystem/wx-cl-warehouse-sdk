<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrder;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrderDetailOutput extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $orderNumber = null;

    /**
     * @var int|null
     */
    public $recordId = null;

    /**
     * @var int
     */
    public $clientId = null;
}
