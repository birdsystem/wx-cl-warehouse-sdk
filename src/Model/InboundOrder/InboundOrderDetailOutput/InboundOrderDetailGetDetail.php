<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderDetailOutput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrderDetailGetDetail extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $orderNumber = null;

    /**
     * @var int|null
     */
    public $recordId = null;

    /**
     * @var int
     */
    public $clientId = null;
}
