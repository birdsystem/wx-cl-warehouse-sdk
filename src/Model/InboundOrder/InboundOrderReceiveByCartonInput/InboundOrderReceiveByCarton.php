<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderReceiveByCartonInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrderReceiveByCarton extends AbstractModel
{
    /**
     * @var string
     */
    public $cartonReference = null;

    /**
     * @var int
     */
    public $cartonQuantity = null;
}
