<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderReceiveInput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrderReceive extends AbstractModel
{
    /**
     * @var
     * \WarehouseX\ClWarehouse\Model\InboundOrderReceiveInputOrderDetail\InboundOrderReceive[]
     */
    public $cartons = null;
}
