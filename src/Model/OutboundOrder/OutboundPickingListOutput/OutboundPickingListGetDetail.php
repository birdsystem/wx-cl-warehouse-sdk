<?php

namespace WarehouseX\ClWarehouse\Model\OutboundOrder\OutboundPickingListOutput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrder.
 */
class OutboundPickingListGetDetail extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $orderNumber = null;

    /**
     * @var int|null
     */
    public $recordId = null;

    /**
     * @var string|null
     */
    public $recordOrderNumber = null;
}
