<?php

namespace WarehouseX\ClWarehouse\Model\OutboundOrder;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrder.
 */
class OutboundOrderPatch extends AbstractModel
{
    /**
     * @var int
     */
    public $logisticsServiceId = null;

    /**
     * @var string|null
     */
    public $trackingNumber = null;

    /**
     * @var string|null
     */
    public $deliveryReference = null;

    /**
     * @var string
     */
    public $status = 'WAIT_PICKING';
}
