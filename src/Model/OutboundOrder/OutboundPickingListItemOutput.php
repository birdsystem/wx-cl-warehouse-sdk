<?php

namespace WarehouseX\ClWarehouse\Model\OutboundOrder;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundOrder.
 */
class OutboundPickingListItemOutput extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $orderNumber = null;

    /**
     * @var int|null
     */
    public $recordId = null;

    /**
     * @var string|null
     */
    public $recordOrderNumber = null;
}
