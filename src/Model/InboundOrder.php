<?php

namespace WarehouseX\ClWarehouse\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrder.
 */
class InboundOrder extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $orderNumber = null;

    /**
     * @var int|null
     */
    public $recordId = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var int
     */
    public $warehouseId = null;

    /**
     * @var int
     */
    public $estimatedCartonQuantity = null;

    /**
     * @var int
     */
    public $actualCartonQuantity = null;

    /**
     * @var int
     */
    public $stockedCartonQuantity = null;

    /**
     * @var string|null
     */
    public $estimatedArrivalTime = null;

    /**
     * @var string|null
     */
    public $receiveTime = null;

    /**
     * @var string|null
     */
    public $stockedTime = null;

    /**
     * @var string|null
     */
    public $cancelReason = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $status = 'WAIT_RECEIVE';

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     */
    public $warehouseName = null;

    /**
     * @var string|null
     */
    public $clientName = null;
}
