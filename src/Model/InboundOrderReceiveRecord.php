<?php

namespace WarehouseX\ClWarehouse\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrderReceiveRecord.
 */
class InboundOrderReceiveRecord extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $cartonId = null;

    /**
     * @var int
     */
    public $userId = null;

    /**
     * @var int
     */
    public $receivedCartonQuantity = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $inboundOrder = null;
}
