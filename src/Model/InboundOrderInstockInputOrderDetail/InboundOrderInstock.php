<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrderInstockInputOrderDetail;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

class InboundOrderInstock extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var int
     */
    public $quantity = null;
}
