<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrderDetail;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * InboundOrderDetail.
 */
class Put extends AbstractModel
{
    /**
     * @var int
     */
    public $estimatedCartonQuantity = null;

    /**
     * @var int
     */
    public $actualCartonQuantity = null;

    /**
     * @var int
     */
    public $stockedCartonQuantity = null;

    /**
     * @var string
     */
    public $status = 'WAIT_RECEIVE';

    /**
     * @var string|null
     */
    public $receiveTime = null;

    /**
     * @var string|null
     */
    public $stockedTime = null;
}
