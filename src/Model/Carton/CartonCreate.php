<?php

namespace WarehouseX\ClWarehouse\Model\Carton;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Carton.
 */
class CartonCreate extends AbstractModel
{
    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $note = null;
}
