<?php

namespace WarehouseX\ClWarehouse\Model\Carton;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Carton.
 */
class CartonInput extends AbstractModel
{
    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $note = null;
}
