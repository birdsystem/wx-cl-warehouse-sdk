<?php

namespace WarehouseX\ClWarehouse\Model\Carton;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Carton.
 */
class OutboundOrderDetailOutput extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var string|null
     */
    public $note = null;
}
