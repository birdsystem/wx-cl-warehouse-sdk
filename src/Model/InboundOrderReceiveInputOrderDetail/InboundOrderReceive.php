<?php

namespace WarehouseX\ClWarehouse\Model\InboundOrderReceiveInputOrderDetail;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

class InboundOrderReceive extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $reference = null;

    /**
     * @var int
     */
    public $quantity = null;
}
