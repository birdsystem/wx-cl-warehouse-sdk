<?php

namespace WarehouseX\ClWarehouse\Model\OutboundPickingListItem\OutboundPickingListOutput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundPickingListItem.
 */
class OutboundPickingListGetDetail extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int|null
     */
    public $userId = null;

    /**
     * @var int
     */
    public $cartonId = null;

    /**
     * @var int
     */
    public $locationId = null;

    /**
     * @var string|null
     */
    public $locationReference = null;

    /**
     * @var int
     */
    public $quantity = null;

    /**
     * @var int
     */
    public $pickedQuantity = null;

    /**
     * @var string
     */
    public $status = 'WAIT_PICKING';

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    public $updateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var
     * \WarehouseX\ClWarehouse\Model\OutboundOrder\OutboundPickingListOutput\OutboundPickingListGetDetail
     */
    public $outboundOrder = null;

    /**
     * @var
     * \WarehouseX\ClWarehouse\Model\Carton\OutboundPickingListOutput\OutboundPickingListGetDetail
     */
    public $carton = null;
}
