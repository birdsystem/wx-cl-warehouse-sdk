<?php

namespace WarehouseX\ClWarehouse\Model\OutboundPickingList;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundPickingList.
 */
class OutboundPickingListModify extends AbstractModel
{
    /**
     * @var int
     */
    public $warehouseId = null;

    /**
     * @var string
     */
    public $status = 'WAIT_PICKING';

    /**
     * @var string
     */
    public $remark = null;
}
