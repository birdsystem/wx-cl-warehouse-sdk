<?php

namespace WarehouseX\ClWarehouse\Model\OutboundPickingList\OutboundPickingListOutput;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * OutboundPickingList.
 */
class OutboundPickingListGetDetail extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $userId = null;

    /**
     * @var int
     */
    public $warehouseId = null;

    /**
     * | null.
     *
     * @var string
     */
    public $warehouseName = null;

    /**
     * @var string
     */
    public $orderNumber = null;

    /**
     * @var int
     */
    public $totalOrderCount = null;

    /**
     * @var int
     */
    public $finishOrderCount = null;

    /**
     * @var int
     */
    public $needPickCartonCount = null;

    /**
     * @var int
     */
    public $hasPickedCartonCount = null;

    /**
     * @var string
     */
    public $status = 'WAIT_PICKING';

    /**
     * @var string
     */
    public $remark = null;

    /**
     * @var string
     */
    public $finishTime = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var
     * \WarehouseX\ClWarehouse\Model\OutboundPickingListItem\OutboundPickingListOutput\OutboundPickingListGetDetail[]
     */
    public $pickingListItems = null;
}
