<?php

namespace WarehouseX\ClWarehouse\Api;

use WarehouseX\ClWarehouse\Model\OutboundOrder\OutboundOrderInput as OutboundOrderInput;
use WarehouseX\ClWarehouse\Model\OutboundOrder\OutboundOrderInput\OutboundOrderPut as OutboundOrderPut;
use WarehouseX\ClWarehouse\Model\OutboundOrder\OutboundOrderOutput as OutboundOrderOutput;
use WarehouseX\ClWarehouse\Model\OutboundOrder\OutboundOrderOutput\OutboundOrderGetDetail as OutboundOrderGetDetail;
use WarehouseX\ClWarehouse\Model\OutboundOrder\OutboundOrderPatch as OutboundOrderPatch;

class OutboundOrder extends AbstractAPI
{
    /**
     * Retrieves the collection of OutboundOrder resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'orderNumber'	string
     *                       'trackingNumber'	string
     *                       'contact'	string
     *                       'companyName'	string
     *                       'telephone'	string
     *                       'county'	string
     *                       'type'	string
     *                       'type[]'	array
     *                       'note'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'recordId'	integer
     *                       'recordId[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'warehouseId'	integer
     *                       'warehouseId[]'	array
     *                       'logisticsServiceId'	integer
     *                       'logisticsServiceId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[orderNumber]'	string
     *                       'order[trackingNumber]'	string
     *                       'order[contact]'	string
     *                       'order[companyName]'	string
     *                       'order[telephone]'	string
     *                       'order[county]'	string
     *                       'order[type]'	string
     *                       'order[note]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return OutboundOrderOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getOutboundOrderCollection',
        'GET',
        'api/cl-warehouse/outbound_orders',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a OutboundOrder resource.
     *
     * @param OutboundOrderInput $Model The new OutboundOrder resource
     *
     * @return OutboundOrderGetDetail
     */
    public function postCollection(OutboundOrderInput $Model): OutboundOrderGetDetail
    {
        return $this->request(
        'postOutboundOrderCollection',
        'POST',
        'api/cl-warehouse/outbound_orders',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a OutboundOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return OutboundOrderGetDetail|null
     */
    public function getItem(string $id): ?OutboundOrderGetDetail
    {
        return $this->request(
        'getOutboundOrderItem',
        'GET',
        "api/cl-warehouse/outbound_orders/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the OutboundOrder resource.
     *
     * @param string           $id    Resource identifier
     * @param OutboundOrderPut $Model The updated OutboundOrder resource
     *
     * @return OutboundOrderGetDetail
     */
    public function putItem(string $id, OutboundOrderPut $Model): OutboundOrderGetDetail
    {
        return $this->request(
        'putOutboundOrderItem',
        'PUT',
        "api/cl-warehouse/outbound_orders/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the OutboundOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteOutboundOrderItem',
        'DELETE',
        "api/cl-warehouse/outbound_orders/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the OutboundOrder resource.
     *
     * @param string             $id    Resource identifier
     * @param OutboundOrderPatch $Model The updated OutboundOrder resource
     *
     * @return OutboundOrderGetDetail
     */
    public function patchItem(string $id, OutboundOrderPatch $Model): OutboundOrderGetDetail
    {
        return $this->request(
        'patchOutboundOrderItem',
        'PATCH',
        "api/cl-warehouse/outbound_orders/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
