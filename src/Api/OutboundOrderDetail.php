<?php

namespace WarehouseX\ClWarehouse\Api;

use WarehouseX\ClWarehouse\Model\OutboundOrderDetail\OutboundOrderDetailOutput as OutboundOrderDetailOutput;

class OutboundOrderDetail extends AbstractAPI
{
    /**
     * Retrieves the collection of OutboundOrderDetail resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'cartonId'	integer
     *                       'cartonId[]'	array
     *                       'quantity'	integer
     *                       'quantity[]'	array
     *                       'quantity[between]'	string
     *                       'quantity[gt]'	string
     *                       'quantity[gte]'	string
     *                       'quantity[lt]'	string
     *                       'quantity[lte]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[cartonId]'	string
     *                       'order[quantity]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return OutboundOrderDetailOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getOutboundOrderDetailCollection',
        'GET',
        'api/cl-warehouse/outbound_order_details',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a OutboundOrderDetail resource.
     *
     * @param string $id Resource identifier
     *
     * @return OutboundOrderDetailOutput|null
     */
    public function getItem(string $id): ?OutboundOrderDetailOutput
    {
        return $this->request(
        'getOutboundOrderDetailItem',
        'GET',
        "api/cl-warehouse/outbound_order_details/$id",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a OutboundOrder resource.
     *
     * @param string $id      OutboundOrder identifier
     * @param array  $queries options:
     *                        'page'	integer	The collection page number
     *                        'itemsPerPage'	integer	The number of items per page
     *                        'status'	string
     *                        'status[]'	array
     *                        'id'	integer
     *                        'id[]'	array
     *                        'cartonId'	integer
     *                        'cartonId[]'	array
     *                        'quantity'	integer
     *                        'quantity[]'	array
     *                        'quantity[between]'	string
     *                        'quantity[gt]'	string
     *                        'quantity[gte]'	string
     *                        'quantity[lt]'	string
     *                        'quantity[lte]'	string
     *                        'createTime[before]'	string
     *                        'createTime[strictly_before]'	string
     *                        'createTime[after]'	string
     *                        'createTime[strictly_after]'	string
     *                        'updateTime[before]'	string
     *                        'updateTime[strictly_before]'	string
     *                        'updateTime[after]'	string
     *                        'updateTime[strictly_after]'	string
     *                        'order[id]'	string
     *                        'order[cartonId]'	string
     *                        'order[quantity]'	string
     *                        'order[status]'	string
     *                        'order[createTime]'	string
     *                        'order[updateTime]'	string
     *
     * @return OutboundOrderDetailOutput[]|null
     */
    public function api_outbound_orders_outbound_order_details_get_subresourceOutboundOrderSubresource(string $id, array $queries = []): ?array
    {
        return $this->request(
        'api_outbound_orders_outbound_order_details_get_subresourceOutboundOrderSubresource',
        'GET',
        "api/cl-warehouse/outbound_orders/$id/outbound_order_details",
        null,
        $queries,
        []
        );
    }
}
