<?php

namespace WarehouseX\ClWarehouse\Api;

use WarehouseX\ClWarehouse\Model\OutboundPickingListItem\OutboundPickingListItemOutput as OutboundPickingListItemOutput;

class OutboundPickingListItem extends AbstractAPI
{
    /**
     * Retrieves the collection of OutboundPickingListItem resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'userId'	integer
     *                       'userId[]'	array
     *                       'cartonId'	integer
     *                       'cartonId[]'	array
     *                       'quantity'	integer
     *                       'quantity[]'	array
     *                       'quantity[between]'	string
     *                       'quantity[gt]'	string
     *                       'quantity[gte]'	string
     *                       'quantity[lt]'	string
     *                       'quantity[lte]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[userId]'	string
     *                       'order[cartonId]'	string
     *                       'order[quantity]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return OutboundPickingListItemOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getOutboundPickingListItemCollection',
        'GET',
        'api/cl-warehouse/outbound_picking_list_items',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a OutboundPickingListItem resource.
     *
     * @param string $id Resource identifier
     *
     * @return OutboundPickingListItemOutput|null
     */
    public function getItem(string $id): ?OutboundPickingListItemOutput
    {
        return $this->request(
        'getOutboundPickingListItemItem',
        'GET',
        "api/cl-warehouse/outbound_picking_list_items/$id",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a OutboundPickingList resource.
     *
     * @param string $id      OutboundPickingList identifier
     * @param array  $queries options:
     *                        'page'	integer	The collection page number
     *                        'itemsPerPage'	integer	The number of items per page
     *                        'status'	string
     *                        'status[]'	array
     *                        'id'	integer
     *                        'id[]'	array
     *                        'userId'	integer
     *                        'userId[]'	array
     *                        'cartonId'	integer
     *                        'cartonId[]'	array
     *                        'quantity'	integer
     *                        'quantity[]'	array
     *                        'quantity[between]'	string
     *                        'quantity[gt]'	string
     *                        'quantity[gte]'	string
     *                        'quantity[lt]'	string
     *                        'quantity[lte]'	string
     *                        'createTime[before]'	string
     *                        'createTime[strictly_before]'	string
     *                        'createTime[after]'	string
     *                        'createTime[strictly_after]'	string
     *                        'updateTime[before]'	string
     *                        'updateTime[strictly_before]'	string
     *                        'updateTime[after]'	string
     *                        'updateTime[strictly_after]'	string
     *                        'order[id]'	string
     *                        'order[userId]'	string
     *                        'order[cartonId]'	string
     *                        'order[quantity]'	string
     *                        'order[status]'	string
     *                        'order[createTime]'	string
     *                        'order[updateTime]'	string
     *
     * @return OutboundPickingListItemOutput[]|null
     */
    public function api_outbound_picking_lists_picking_list_items_get_subresourceOutboundPickingListSubresource(string $id, array $queries = []): ?array
    {
        return $this->request(
        'api_outbound_picking_lists_picking_list_items_get_subresourceOutboundPickingListSubresource',
        'GET',
        "api/cl-warehouse/outbound_picking_lists/$id/picking_list_items",
        null,
        $queries,
        []
        );
    }
}
