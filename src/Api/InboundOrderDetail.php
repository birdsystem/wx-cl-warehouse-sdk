<?php

namespace WarehouseX\ClWarehouse\Api;

use WarehouseX\ClWarehouse\Model\InboundOrderDetail as InboundOrderDetailModel;
use WarehouseX\ClWarehouse\Model\InboundOrderDetail\InboundOrderDetailOutput as InboundOrderDetailOutput;
use WarehouseX\ClWarehouse\Model\InboundOrderDetail\InboundOrderDetailOutput\InboundOrderDetailGetDetail as InboundOrderDetailGetDetail;
use WarehouseX\ClWarehouse\Model\InboundOrderDetail\Patch as Patch;
use WarehouseX\ClWarehouse\Model\InboundOrderDetail\Put as Put;

class InboundOrderDetail extends AbstractAPI
{
    /**
     * Retrieves the collection of InboundOrderDetail resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'inboundOrder.id'	integer
     *                       'inboundOrder.id[]'	array
     *                       'carton.id'	integer
     *                       'carton.id[]'	array
     *                       'estimatedCartonQuantity'	integer
     *                       'estimatedCartonQuantity[]'	array
     *                       'actualCartonQuantity'	integer
     *                       'actualCartonQuantity[]'	array
     *                       'stockedCartonQuantity'	integer
     *                       'stockedCartonQuantity[]'	array
     *                       'estimatedCartonQuantity[between]'	string
     *                       'estimatedCartonQuantity[gt]'	string
     *                       'estimatedCartonQuantity[gte]'	string
     *                       'estimatedCartonQuantity[lt]'	string
     *                       'estimatedCartonQuantity[lte]'	string
     *                       'actualCartonQuantity[between]'	string
     *                       'actualCartonQuantity[gt]'	string
     *                       'actualCartonQuantity[gte]'	string
     *                       'actualCartonQuantity[lt]'	string
     *                       'actualCartonQuantity[lte]'	string
     *                       'stockedCartonQuantity[between]'	string
     *                       'stockedCartonQuantity[gt]'	string
     *                       'stockedCartonQuantity[gte]'	string
     *                       'stockedCartonQuantity[lt]'	string
     *                       'stockedCartonQuantity[lte]'	string
     *                       'receiveTime[before]'	string
     *                       'receiveTime[strictly_before]'	string
     *                       'receiveTime[after]'	string
     *                       'receiveTime[strictly_after]'	string
     *                       'stockedTime[before]'	string
     *                       'stockedTime[strictly_before]'	string
     *                       'stockedTime[after]'	string
     *                       'stockedTime[strictly_after]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[inboundOrder.id]'	string
     *                       'order[carton.id]'	string
     *                       'order[estimatedCartonQuantity]'	string
     *                       'order[actualCartonQuantity]'	string
     *                       'order[stockedCartonQuantity]'	string
     *                       'order[status]'	string
     *                       'order[receiveTime]'	string
     *                       'order[stockedTime]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return InboundOrderDetailOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getInboundOrderDetailCollection',
        'GET',
        'api/cl-warehouse/inbound_order_details',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a InboundOrderDetail resource.
     *
     * @param InboundOrderDetailModel $Model The new InboundOrderDetail resource
     *
     * @return InboundOrderDetailGetDetail
     */
    public function postCollection(InboundOrderDetailModel $Model): InboundOrderDetailGetDetail
    {
        return $this->request(
        'postInboundOrderDetailCollection',
        'POST',
        'api/cl-warehouse/inbound_order_details',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a InboundOrderDetail resource.
     *
     * @param string $id Resource identifier
     *
     * @return InboundOrderDetailGetDetail|null
     */
    public function getItem(string $id): ?InboundOrderDetailGetDetail
    {
        return $this->request(
        'getInboundOrderDetailItem',
        'GET',
        "api/cl-warehouse/inbound_order_details/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the InboundOrderDetail resource.
     *
     * @param string $id    Resource identifier
     * @param Put    $Model The updated InboundOrderDetail resource
     *
     * @return InboundOrderDetailGetDetail
     */
    public function putItem(string $id, Put $Model): InboundOrderDetailGetDetail
    {
        return $this->request(
        'putInboundOrderDetailItem',
        'PUT',
        "api/cl-warehouse/inbound_order_details/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the InboundOrderDetail resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteInboundOrderDetailItem',
        'DELETE',
        "api/cl-warehouse/inbound_order_details/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the InboundOrderDetail resource.
     *
     * @param string $id    Resource identifier
     * @param Patch  $Model The updated InboundOrderDetail resource
     *
     * @return InboundOrderDetailGetDetail
     */
    public function patchItem(string $id, Patch $Model): InboundOrderDetailGetDetail
    {
        return $this->request(
        'patchInboundOrderDetailItem',
        'PATCH',
        "api/cl-warehouse/inbound_order_details/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a InboundOrder resource.
     *
     * @param string $id      InboundOrder identifier
     * @param array  $queries options:
     *                        'page'	integer	The collection page number
     *                        'itemsPerPage'	integer	The number of items per page
     *                        'status'	string
     *                        'status[]'	array
     *                        'id'	integer
     *                        'id[]'	array
     *                        'inboundOrder.id'	integer
     *                        'inboundOrder.id[]'	array
     *                        'carton.id'	integer
     *                        'carton.id[]'	array
     *                        'estimatedCartonQuantity'	integer
     *                        'estimatedCartonQuantity[]'	array
     *                        'actualCartonQuantity'	integer
     *                        'actualCartonQuantity[]'	array
     *                        'stockedCartonQuantity'	integer
     *                        'stockedCartonQuantity[]'	array
     *                        'estimatedCartonQuantity[between]'	string
     *                        'estimatedCartonQuantity[gt]'	string
     *                        'estimatedCartonQuantity[gte]'	string
     *                        'estimatedCartonQuantity[lt]'	string
     *                        'estimatedCartonQuantity[lte]'	string
     *                        'actualCartonQuantity[between]'	string
     *                        'actualCartonQuantity[gt]'	string
     *                        'actualCartonQuantity[gte]'	string
     *                        'actualCartonQuantity[lt]'	string
     *                        'actualCartonQuantity[lte]'	string
     *                        'stockedCartonQuantity[between]'	string
     *                        'stockedCartonQuantity[gt]'	string
     *                        'stockedCartonQuantity[gte]'	string
     *                        'stockedCartonQuantity[lt]'	string
     *                        'stockedCartonQuantity[lte]'	string
     *                        'receiveTime[before]'	string
     *                        'receiveTime[strictly_before]'	string
     *                        'receiveTime[after]'	string
     *                        'receiveTime[strictly_after]'	string
     *                        'stockedTime[before]'	string
     *                        'stockedTime[strictly_before]'	string
     *                        'stockedTime[after]'	string
     *                        'stockedTime[strictly_after]'	string
     *                        'createTime[before]'	string
     *                        'createTime[strictly_before]'	string
     *                        'createTime[after]'	string
     *                        'createTime[strictly_after]'	string
     *                        'updateTime[before]'	string
     *                        'updateTime[strictly_before]'	string
     *                        'updateTime[after]'	string
     *                        'updateTime[strictly_after]'	string
     *                        'order[id]'	string
     *                        'order[inboundOrder.id]'	string
     *                        'order[carton.id]'	string
     *                        'order[estimatedCartonQuantity]'	string
     *                        'order[actualCartonQuantity]'	string
     *                        'order[stockedCartonQuantity]'	string
     *                        'order[status]'	string
     *                        'order[receiveTime]'	string
     *                        'order[stockedTime]'	string
     *                        'order[createTime]'	string
     *                        'order[updateTime]'	string
     *
     * @return InboundOrderDetailOutput[]|null
     */
    public function api_inbound_orders_inbound_order_details_get_subresourceInboundOrderSubresource(string $id, array $queries = []): ?array
    {
        return $this->request(
        'api_inbound_orders_inbound_order_details_get_subresourceInboundOrderSubresource',
        'GET',
        "api/cl-warehouse/inbound_orders/$id/inbound_order_details",
        null,
        $queries,
        []
        );
    }
}
