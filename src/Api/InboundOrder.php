<?php

namespace WarehouseX\ClWarehouse\Api;

use WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderInput\InboundOrderInput\InboundOrderPost as InboundOrderPost;
use WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderInput\InboundOrderInput\InboundOrderPut as InboundOrderPut;
use WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderInstockByCartonInput\InboundOrderInstockByCarton as InboundOrderInstockByCarton;
use WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderInstockInput\InboundOrderInstock as InboundOrderInstock;
use WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderOutput as InboundOrderOutput;
use WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderOutput\InboundOrderGetDetail as InboundOrderGetDetail;
use WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderReceiveByCartonInput\InboundOrderReceiveByCarton as InboundOrderReceiveByCarton;
use WarehouseX\ClWarehouse\Model\InboundOrder\InboundOrderReceiveInput\InboundOrderReceive as InboundOrderReceive;
use WarehouseX\ClWarehouse\Model\InboundOrder\Patch as Patch;

class InboundOrder extends AbstractAPI
{
    /**
     * Retrieves the collection of InboundOrder resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'orderNumber'	string
     *                       'type'	string
     *                       'type[]'	array
     *                       'note'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'inboundOrderDetails.carton.reference'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'recordId'	integer
     *                       'recordId[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'warehouseId'	integer
     *                       'warehouseId[]'	array
     *                       'estimatedCartonQuantity'	integer
     *                       'estimatedCartonQuantity[]'	array
     *                       'actualCartonQuantity'	integer
     *                       'actualCartonQuantity[]'	array
     *                       'stockedCartonQuantity'	integer
     *                       'stockedCartonQuantity[]'	array
     *                       'estimatedCartonQuantity[between]'	string
     *                       'estimatedCartonQuantity[gt]'	string
     *                       'estimatedCartonQuantity[gte]'	string
     *                       'estimatedCartonQuantity[lt]'	string
     *                       'estimatedCartonQuantity[lte]'	string
     *                       'actualCartonQuantity[between]'	string
     *                       'actualCartonQuantity[gt]'	string
     *                       'actualCartonQuantity[gte]'	string
     *                       'actualCartonQuantity[lt]'	string
     *                       'actualCartonQuantity[lte]'	string
     *                       'stockedCartonQuantity[between]'	string
     *                       'stockedCartonQuantity[gt]'	string
     *                       'stockedCartonQuantity[gte]'	string
     *                       'stockedCartonQuantity[lt]'	string
     *                       'stockedCartonQuantity[lte]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'estimatedArrivalTime[before]'	string
     *                       'estimatedArrivalTime[strictly_before]'	string
     *                       'estimatedArrivalTime[after]'	string
     *                       'estimatedArrivalTime[strictly_after]'	string
     *                       'receiveTime[before]'	string
     *                       'receiveTime[strictly_before]'	string
     *                       'receiveTime[after]'	string
     *                       'receiveTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[orderNumber]'	string
     *                       'order[clientId]'	string
     *                       'order[warehouseId]'	string
     *                       'order[estimatedCartonQuantity]'	string
     *                       'order[actualCartonQuantity]'	string
     *                       'order[stockedCartonQuantity]'	string
     *                       'order[estimatedArrivalTime]'	string
     *                       'order[receiveTime]'	string
     *                       'order[status]'	string
     *                       'order[type]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return InboundOrderOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getInboundOrderCollection',
        'GET',
        'api/cl-warehouse/inbound_orders',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a InboundOrder resource.
     *
     * @param InboundOrderPost $Model The new InboundOrder resource
     *
     * @return InboundOrderGetDetail
     */
    public function postCollection(InboundOrderPost $Model): InboundOrderGetDetail
    {
        return $this->request(
        'postInboundOrderCollection',
        'POST',
        'api/cl-warehouse/inbound_orders',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Replaces the InboundOrder resource.
     *
     * @return InboundOrderOutput[]
     */
    public function finish_instockCollection(): array
    {
        return $this->request(
        'finish_instockInboundOrderCollection',
        'PUT',
        'api/cl-warehouse/inbound_orders/finish_instock',
        null,
        [],
        []
        );
    }

    /**
     * Replaces the InboundOrder resource.
     *
     * @return InboundOrderOutput[]
     */
    public function finish_receiveCollection(): array
    {
        return $this->request(
        'finish_receiveInboundOrderCollection',
        'PUT',
        'api/cl-warehouse/inbound_orders/finish_receive',
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a InboundOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return InboundOrderGetDetail|null
     */
    public function getItem(string $id): ?InboundOrderGetDetail
    {
        return $this->request(
        'getInboundOrderItem',
        'GET',
        "api/cl-warehouse/inbound_orders/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the InboundOrder resource.
     *
     * @param string          $id    Resource identifier
     * @param InboundOrderPut $Model The updated InboundOrder resource
     *
     * @return InboundOrderGetDetail
     */
    public function putItem(string $id, InboundOrderPut $Model): InboundOrderGetDetail
    {
        return $this->request(
        'putInboundOrderItem',
        'PUT',
        "api/cl-warehouse/inbound_orders/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the InboundOrder resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteInboundOrderItem',
        'DELETE',
        "api/cl-warehouse/inbound_orders/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the InboundOrder resource.
     *
     * @param string $id    Resource identifier
     * @param Patch  $Model The updated InboundOrder resource
     *
     * @return InboundOrderGetDetail
     */
    public function patchItem(string $id, Patch $Model): InboundOrderGetDetail
    {
        return $this->request(
        'patchInboundOrderItem',
        'PATCH',
        "api/cl-warehouse/inbound_orders/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Replaces the InboundOrder resource.
     *
     * @param string              $id    Resource identifier
     * @param InboundOrderInstock $Model The updated InboundOrder resource
     *
     * @return InboundOrderGetDetail
     */
    public function instockItem(string $id, InboundOrderInstock $Model): InboundOrderGetDetail
    {
        return $this->request(
        'instockInboundOrderItem',
        'PUT',
        "api/cl-warehouse/inbound_orders/$id/instock",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Updates the InboundOrder resource.
     *
     * @param string                      $id    Resource identifier
     * @param InboundOrderInstockByCarton $Model The updated InboundOrder resource
     *
     * @return InboundOrderGetDetail
     */
    public function instock_by_cartonItem(string $id, InboundOrderInstockByCarton $Model): InboundOrderGetDetail
    {
        return $this->request(
        'instock_by_cartonInboundOrderItem',
        'PATCH',
        "api/cl-warehouse/inbound_orders/$id/instock_by_carton",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Replaces the InboundOrder resource.
     *
     * @param string              $id    Resource identifier
     * @param InboundOrderReceive $Model The updated InboundOrder resource
     *
     * @return InboundOrderGetDetail
     */
    public function receiveItem(string $id, InboundOrderReceive $Model): InboundOrderGetDetail
    {
        return $this->request(
        'receiveInboundOrderItem',
        'PUT',
        "api/cl-warehouse/inbound_orders/$id/receive",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Updates the InboundOrder resource.
     *
     * @param string                      $id    Resource identifier
     * @param InboundOrderReceiveByCarton $Model The updated InboundOrder resource
     *
     * @return InboundOrderGetDetail
     */
    public function receive_by_cartonItem(string $id, InboundOrderReceiveByCarton $Model): InboundOrderGetDetail
    {
        return $this->request(
        'receive_by_cartonInboundOrderItem',
        'PATCH',
        "api/cl-warehouse/inbound_orders/$id/receive_by_carton",
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
