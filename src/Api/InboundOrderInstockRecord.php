<?php

namespace WarehouseX\ClWarehouse\Api;

use WarehouseX\ClWarehouse\Model\InboundOrderInstockRecord as InboundOrderInstockRecordModel;

class InboundOrderInstockRecord extends AbstractAPI
{
    /**
     * Retrieves the collection of InboundOrderInstockRecord resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'containerReference'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'inboundOrder.id'	integer
     *                       'inboundOrder.id[]'	array
     *                       'cartonId'	integer
     *                       'cartonId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[inboundOrder.id]'	string
     *                       'order[cartonId]'	string
     *                       'order[containerReference]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return InboundOrderInstockRecordModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getInboundOrderInstockRecordCollection',
        'GET',
        'api/cl-warehouse/inbound_order_instock_records',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a InboundOrderInstockRecord resource.
     *
     * @param InboundOrderInstockRecordModel $Model The new InboundOrderInstockRecord
     *                                              resource
     *
     * @return InboundOrderInstockRecordModel
     */
    public function postCollection(InboundOrderInstockRecordModel $Model): InboundOrderInstockRecordModel
    {
        return $this->request(
        'postInboundOrderInstockRecordCollection',
        'POST',
        'api/cl-warehouse/inbound_order_instock_records',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a InboundOrderInstockRecord resource.
     *
     * @param string $id Resource identifier
     *
     * @return InboundOrderInstockRecordModel|null
     */
    public function getItem(string $id): ?InboundOrderInstockRecordModel
    {
        return $this->request(
        'getInboundOrderInstockRecordItem',
        'GET',
        "api/cl-warehouse/inbound_order_instock_records/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the InboundOrderInstockRecord resource.
     *
     * @param string                         $id    Resource identifier
     * @param InboundOrderInstockRecordModel $Model The updated
     *                                              InboundOrderInstockRecord resource
     *
     * @return InboundOrderInstockRecordModel
     */
    public function putItem(string $id, InboundOrderInstockRecordModel $Model): InboundOrderInstockRecordModel
    {
        return $this->request(
        'putInboundOrderInstockRecordItem',
        'PUT',
        "api/cl-warehouse/inbound_order_instock_records/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the InboundOrderInstockRecord resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteInboundOrderInstockRecordItem',
        'DELETE',
        "api/cl-warehouse/inbound_order_instock_records/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the InboundOrderInstockRecord resource.
     *
     * @param string                         $id    Resource identifier
     * @param InboundOrderInstockRecordModel $Model The updated
     *                                              InboundOrderInstockRecord resource
     *
     * @return InboundOrderInstockRecordModel
     */
    public function patchItem(string $id, InboundOrderInstockRecordModel $Model): InboundOrderInstockRecordModel
    {
        return $this->request(
        'patchInboundOrderInstockRecordItem',
        'PATCH',
        "api/cl-warehouse/inbound_order_instock_records/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
