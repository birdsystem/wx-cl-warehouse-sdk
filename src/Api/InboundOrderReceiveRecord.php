<?php

namespace WarehouseX\ClWarehouse\Api;

use WarehouseX\ClWarehouse\Model\InboundOrderReceiveRecord as InboundOrderReceiveRecordModel;
use WarehouseX\ClWarehouse\Model\InboundOrderReceiveRecord\InboundOrderReceiveRecordOutput as InboundOrderReceiveRecordOutput;

class InboundOrderReceiveRecord extends AbstractAPI
{
    /**
     * Retrieves the collection of InboundOrderReceiveRecord resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'inboundOrder.id'	integer
     *                       'inboundOrder.id[]'	array
     *                       'cartonId'	integer
     *                       'cartonId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[inboundOrder.id]'	string
     *                       'order[cartonId]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return InboundOrderReceiveRecordOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getInboundOrderReceiveRecordCollection',
        'GET',
        'api/cl-warehouse/inbound_order_receive_records',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a InboundOrderReceiveRecord resource.
     *
     * @param InboundOrderReceiveRecordModel $Model The new InboundOrderReceiveRecord
     *                                              resource
     *
     * @return InboundOrderReceiveRecordOutput
     */
    public function postCollection(InboundOrderReceiveRecordModel $Model): InboundOrderReceiveRecordOutput
    {
        return $this->request(
        'postInboundOrderReceiveRecordCollection',
        'POST',
        'api/cl-warehouse/inbound_order_receive_records',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a InboundOrderReceiveRecord resource.
     *
     * @param string $id Resource identifier
     *
     * @return InboundOrderReceiveRecordOutput|null
     */
    public function getItem(string $id): ?InboundOrderReceiveRecordOutput
    {
        return $this->request(
        'getInboundOrderReceiveRecordItem',
        'GET',
        "api/cl-warehouse/inbound_order_receive_records/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the InboundOrderReceiveRecord resource.
     *
     * @param string                         $id    Resource identifier
     * @param InboundOrderReceiveRecordModel $Model The updated
     *                                              InboundOrderReceiveRecord resource
     *
     * @return InboundOrderReceiveRecordOutput
     */
    public function putItem(string $id, InboundOrderReceiveRecordModel $Model): InboundOrderReceiveRecordOutput
    {
        return $this->request(
        'putInboundOrderReceiveRecordItem',
        'PUT',
        "api/cl-warehouse/inbound_order_receive_records/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the InboundOrderReceiveRecord resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteInboundOrderReceiveRecordItem',
        'DELETE',
        "api/cl-warehouse/inbound_order_receive_records/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the InboundOrderReceiveRecord resource.
     *
     * @param string                         $id    Resource identifier
     * @param InboundOrderReceiveRecordModel $Model The updated
     *                                              InboundOrderReceiveRecord resource
     *
     * @return InboundOrderReceiveRecordOutput
     */
    public function patchItem(string $id, InboundOrderReceiveRecordModel $Model): InboundOrderReceiveRecordOutput
    {
        return $this->request(
        'patchInboundOrderReceiveRecordItem',
        'PATCH',
        "api/cl-warehouse/inbound_order_receive_records/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
