<?php

namespace WarehouseX\ClWarehouse\Api;

use WarehouseX\ClWarehouse\Model\Carton\CartonCreate as CartonCreate;
use WarehouseX\ClWarehouse\Model\Carton\CartonInput as CartonInput;
use WarehouseX\ClWarehouse\Model\Carton\CartonOutput as CartonOutput;

class Carton extends AbstractAPI
{
    /**
     * Retrieves the collection of Carton resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'reference'	string
     *                       'type'	string
     *                       'type[]'	array
     *                       'note'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'parentId'	integer
     *                       'parentId[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[reference]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return CartonOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getCartonCollection',
        'GET',
        'api/cl-warehouse/cartons',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Carton resource.
     *
     * @param CartonCreate $Model The new Carton resource
     *
     * @return CartonOutput
     */
    public function postCollection(CartonCreate $Model): CartonOutput
    {
        return $this->request(
        'postCartonCollection',
        'POST',
        'api/cl-warehouse/cartons',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of Carton resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'reference'	string
     *                       'type'	string
     *                       'type[]'	array
     *                       'note'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'parentId'	integer
     *                       'parentId[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[reference]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return CartonOutput[]|null
     */
    public function generate_referenceCollection(array $queries = []): ?array
    {
        return $this->request(
        'generate_referenceCartonCollection',
        'GET',
        'api/cl-warehouse/cartons/generate-reference',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Carton resource.
     *
     * @return CartonOutput
     */
    public function import_by_fileCollection(): CartonOutput
    {
        return $this->request(
        'import_by_fileCartonCollection',
        'POST',
        'api/cl-warehouse/cartons/import-by-file',
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a Carton resource.
     *
     * @param string $id Resource identifier
     *
     * @return CartonOutput|null
     */
    public function getItem(string $id): ?CartonOutput
    {
        return $this->request(
        'getCartonItem',
        'GET',
        "api/cl-warehouse/cartons/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Carton resource.
     *
     * @param string      $id    Resource identifier
     * @param CartonInput $Model The updated Carton resource
     *
     * @return CartonOutput
     */
    public function putItem(string $id, CartonInput $Model): CartonOutput
    {
        return $this->request(
        'putCartonItem',
        'PUT',
        "api/cl-warehouse/cartons/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Carton resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteCartonItem',
        'DELETE',
        "api/cl-warehouse/cartons/$id",
        null,
        [],
        []
        );
    }
}
