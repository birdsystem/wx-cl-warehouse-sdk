<?php

namespace WarehouseX\ClWarehouse\Api;

use WarehouseX\ClWarehouse\Model\OutboundPickingList\OutboundPickingListModify as OutboundPickingListModify;
use WarehouseX\ClWarehouse\Model\OutboundPickingList\OutboundPickingListOutput as OutboundPickingListOutput;
use WarehouseX\ClWarehouse\Model\OutboundPickingList\OutboundPickingListOutput\OutboundPickingListGetDetail as OutboundPickingListGetDetail;

class OutboundPickingList extends AbstractAPI
{
    /**
     * Retrieves the collection of OutboundPickingList resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'orderNumber'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'remark'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'warehouseId'	integer
     *                       'warehouseId[]'	array
     *                       'id[between]'	string
     *                       'id[gt]'	string
     *                       'id[gte]'	string
     *                       'id[lt]'	string
     *                       'id[lte]'	string
     *                       'userId[between]'	string
     *                       'userId[gt]'	string
     *                       'userId[gte]'	string
     *                       'userId[lt]'	string
     *                       'userId[lte]'	string
     *                       'warehouseId[between]'	string
     *                       'warehouseId[gt]'	string
     *                       'warehouseId[gte]'	string
     *                       'warehouseId[lt]'	string
     *                       'warehouseId[lte]'	string
     *                       'orderNumber[between]'	string
     *                       'orderNumber[gt]'	string
     *                       'orderNumber[gte]'	string
     *                       'orderNumber[lt]'	string
     *                       'orderNumber[lte]'	string
     *                       'status[between]'	string
     *                       'status[gt]'	string
     *                       'status[gte]'	string
     *                       'status[lt]'	string
     *                       'status[lte]'	string
     *                       'remark[between]'	string
     *                       'remark[gt]'	string
     *                       'remark[gte]'	string
     *                       'remark[lt]'	string
     *                       'remark[lte]'	string
     *                       'finishTime[between]'	string
     *                       'finishTime[gt]'	string
     *                       'finishTime[gte]'	string
     *                       'finishTime[lt]'	string
     *                       'finishTime[lte]'	string
     *                       'createTime[between]'	string
     *                       'createTime[gt]'	string
     *                       'createTime[gte]'	string
     *                       'createTime[lt]'	string
     *                       'createTime[lte]'	string
     *                       'updateTime[between]'	string
     *                       'updateTime[gt]'	string
     *                       'updateTime[gte]'	string
     *                       'updateTime[lt]'	string
     *                       'updateTime[lte]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[warehouseId]'	string
     *                       'order[status]'	string
     *                       'order[remark]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return OutboundPickingListOutput[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getOutboundPickingListCollection',
        'GET',
        'api/cl-warehouse/outbound_picking_lists',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a OutboundPickingList resource.
     *
     * @return OutboundPickingListOutput
     */
    public function generateCollection(): OutboundPickingListOutput
    {
        return $this->request(
        'generateOutboundPickingListCollection',
        'POST',
        'api/cl-warehouse/outbound_picking_lists/generate',
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a OutboundPickingList resource.
     *
     * @param string $id Resource identifier
     *
     * @return OutboundPickingListGetDetail|null
     */
    public function getItem(string $id): ?OutboundPickingListGetDetail
    {
        return $this->request(
        'getOutboundPickingListItem',
        'GET',
        "api/cl-warehouse/outbound_picking_lists/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the OutboundPickingList resource.
     *
     * @param string                    $id    Resource identifier
     * @param OutboundPickingListModify $Model The updated OutboundPickingList resource
     *
     * @return OutboundPickingListOutput
     */
    public function putItem(string $id, OutboundPickingListModify $Model): OutboundPickingListOutput
    {
        return $this->request(
        'putOutboundPickingListItem',
        'PUT',
        "api/cl-warehouse/outbound_picking_lists/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the OutboundPickingList resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteOutboundPickingListItem',
        'DELETE',
        "api/cl-warehouse/outbound_picking_lists/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the OutboundPickingList resource.
     *
     * @param string $id Resource identifier
     *
     * @return OutboundPickingListOutput
     */
    public function pick_itemItem(string $id): OutboundPickingListOutput
    {
        return $this->request(
        'pick_itemOutboundPickingListItem',
        'PUT',
        "api/cl-warehouse/outbound_picking_lists/$id/pick-item",
        null,
        [],
        []
        );
    }
}
